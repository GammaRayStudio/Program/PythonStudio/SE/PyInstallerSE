PyInstaller
======
`將 Python 打包封裝成執行檔`

安裝
------
### python 2

    pip install pyinstaller

### python 3

    pip3 install pyinstaller


參數
------
### 查看參數

    pyinstaller -h

+ -F : 打包成 exe 
+ -icon : 圖標路徑
+ -w : 使用視窗，無控制台
+ -c : 使用控制台，無視窗
+ -D : 創建目錄，包含依賴文件
+ --noconsole : 無控制台

### 打包範例

**Sample Code : sample.py**
```python
print("Hello , Pyinstaller !")
input()
```

**Windows**

    pyinstaller -F .\sample.py

**Mac**

    pyinstaller -F ./sample.py



Reference
------
### 【Python】使用 PyInstaller 將 Python打包成 exe 檔
<https://medium.com/pyladies-taiwan/python-%E5%B0%87python%E6%89%93%E5%8C%85%E6%88%90exe%E6%AA%94-32a4bacbe351>

### Python 程式打包為執行檔.exe ( Mac OS )
<https://medium.com/%E6%88%91%E5%B0%B1%E5%95%8F%E4%B8%80%E5%8F%A5-%E6%80%8E%E9%BA%BC%E5%AF%AB/python-%E5%B0%87%E7%A8%8B%E5%BC%8F%E6%89%93%E5%8C%85%E7%82%BA%E5%9F%B7%E8%A1%8C%E6%AA%94-exe-mac-os-e9521bc87e24>

### Pyinstaller 使用+打包圖片方法
<https://www.itread01.com/content/1544898606.html>






